from utils.mongo_utils import get_pymongo_client
from utils.data_utils import read_population_jsonfile, load_data
from concurrent.futures import ThreadPoolExecutor
from utils.datetime_utils import subtract_one_day
from tqdm import tqdm
from utils.data_utils import df2dict, dict2df
from utils.datetime_utils import date2ts
import pandas as pd

FILEPATH = 'data/data.min.json'


def update_district_population_value(collection, population_collection):
    client = get_pymongo_client()
    db = client.get_database('dbexplain')
    coll = db.get_collection(collection)
    population_coll = db.get_collection(population_collection)
    district_population = {}
    for record in population_coll.find():
        district = record['District']
        population = record['Population']
        district_population[district] = population
    # def _update(record):
    #     district = record['District']
    #     population = 0
    #     if district in district_population.keys():
    #         population = district_population[district]
    #
    #     coll.update_one({'District': district},
    #                     {
    #                         '$set': {
    #                             'Population': population
    #                         }
    #                     })
    # with ThreadPoolExecutor() as executor:
    #     for _ in executor.map(lambda x: _update(x), records):
    #         x = 2
    seen_districts = []
    for record in tqdm(coll.find()):
        district = record['District']
        if district  in seen_districts:
            continue
        seen_districts.append(district)
        population = 0
        if district in district_population.keys():
            population = district_population[district]

        coll.update_many({'District': district},
                        {
                            '$set':  {'Population': population}
                        })


def update_state_population_value(collection, population_collection):
    client = get_pymongo_client()
    db = client.get_database('dbexplain')
    coll = db.get_collection(collection)
    population_coll = db.get_collection(population_collection)
    for record in coll.find():

        population_record = population_coll.find_one({'State': record['State']})
        print(population_record)
        coll.update_one({'State': record['State']},
                        {'$set': {
                            'Population': population_record['Population']}})


def prepare_district_population_records(filepath):
    """"""
    d = read_population_jsonfile(filepath)
    records = []
    for state_str in d.keys():
        state = d[state_str]
        if 'districts' not in state.keys():
            continue
        for district_str in state['districts'].keys():
            district = state['districts'][district_str]
            population = 0
            if 'meta' in district.keys() and 'population' in district[
                'meta'].keys():
                population = district['meta']['population']
            record = {
                'State': state_str,
                'District': district_str,
                'Population': population
            }
            records.append(record)
    return records


def prepare_state_population_records(filepath):
    """"""
    d = read_population_jsonfile(filepath)
    records = []
    for state_str in d.keys():
        state = d[state_str]
        if 'meta' in state.keys() and 'population' in state['meta'].keys():
            population = state['meta']['population']
            record = {
                'State': state_str,
                'Population': population
            }
            records.append(record)
    return records


def prepare_district_vaccination_records(filepath):
    """
    Total Individuals Registered
    Sessions
    Sites
    First Dose Administered  == date.3
    Second Dose Administered == date.4
    Male(Individuals Vaccinated)
    Female(Individuals Vaccinated)
    Transgender(Individuals Vaccinated)
    Covaxin (Doses Administered)
    CoviShield (Doses Administered)
    """
    df = pd.read_csv(filepath)
    columns = list(df.columns)
    records = []
    for iter, row in df.iterrows():
        state = row['State']
        district = row['District']
        for col in columns[6:]:
            if '.3' in col:
                vaccination1 = row[col]
                date = col.split('.3')[0]
            if '.4' in col:
                vaccination2 = row[col]
                date = col.split('.4')[0]
                record = {
                    'Date': date,
                    'State': state,
                    'DateTs': date2ts(date, '%d/%m/%y'),
                    'District': district,
                    'vaccination1': vaccination1,
                    'vaccination2': vaccination2
                }
                records.append(record)
    return records

# Vaccinated As of,State,First Dose Administered,Second Dose Administered,Total Doses Administered
def prepare_state_vaccination_records(filepath):
    df = pd.read_csv(filepath)
    records = []
    for iter, row in df.iterrows():
        date = row['Vaccinated As of']
        date_ts = date2ts(date, '%d/%m/%Y')
        state = row['State']
        vaccinated1 = row['First Dose Administered']
        vaccinated2 = row['Second Dose Administered']
        population = 0  # ??
        total = row['Total Doses Administered']
        record = {
            'Date': date,
            'DateTs': date_ts,
            'State': state,
            'Vaccinated1': vaccinated1,
            'Vaccinated2': vaccinated2
        }
        records.append(record)
    return records


def insert_records(records, dbname='dbexplain',
                   collection='district_vaccination'):
    client = get_pymongo_client()
    db = client.get_database(dbname)
    coll = db.get_collection(collection)
    for record in records:
        coll.insert_one(record)
    print(f'inserted {len(records)} records!')


def active_case_records(csv_filepath):
    df = load_data(csv_filepath)
    df['Active'] = df['Confirmed'] - df['Recovered'] - df['Deceased']
    records = []
    for iter, row in df.iterrows():
        record = {
            'District': row['District'],
            'Date': row['Date'],
            'DateTs': date2ts(row['Date'], '%Y-%m-%d'),
            'State': row['State'], 'Confirmed': row['Confirmed'],
            'Recovered': row['Recovered'],
            'Deceased': row['Deceased'],
            'Active': row['Active']
        }
        records.append(record)
    return records


if __name__ == '__main__':


    # records = active_case_records('data/districts.csv')
    # insert_records(records, dbname='dbexplain', collection='district')

    # records = prepare_state_vaccination_records('data/vaccine_doses_administered_statewise.csv')
    # insert_records(records, dbname='dbexplain',
    #                collection='state_vaccination')
    # records = active_case_records('data/states.csv')
    # insert_records(records, dbname='dbexplain', collection='state')

    # records = prepare_district_vaccination_records('data/vaccination_district_wise.csv')
    # insert_records(records, 'dbexplain', 'district_vaccination_new')

    # records = prepare_state_population_records('data/data.min.json')
    # insert_records(records, 'dbexplain', 'state_population')
    #
    # records = prepare_district_population_records('data/data.min.json')
    # insert_records(records, 'dbexplain', 'district_population')

    # update_district_population_value('district_vaccination_new',
    #                                  'district_population')

    x = 2