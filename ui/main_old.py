import base64
import datetime
import io
import numpy as np
from datetime import date
from dash import Dash, html, dcc, dash_table
import plotly.express as px
import pandas as pd
from dash.dependencies import Input, Output, State
from utils.data_utils import load_data, load_preprocess_india_data

external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']

app = Dash(__name__, external_stylesheets=external_stylesheets)

TS_COL_NAME = 'Active'


def parse_contents_india_data(contents, filename, date):
    content_type, content_string = contents.split(',')
    decoded = base64.b64decode(content_string)
    df = None
    if '.csv' in filename:
        df = load_preprocess_india_data(io.StringIO(decoded.decode('utf-8')))
    else:
        return df
    df_ts = df[['Date', TS_COL_NAME]]
    print(f'we got the df_ts')
    df_ts = df_ts.groupby(['Date']).count()
    print(f'Aggregation done.')
    fig = px.line(df_ts)
    print(f'Figure plot...rendering')
    return html.Div([
        dcc.Graph(figure=fig),
        html.Hr()
    ])


# def parse_contents(contents, filename, date):
#     content_type, content_string = contents.split(',')
#     decoded = base64.b64decode(content_string)
#     df = None
#     # TODO(rajiv): Extend this to other file extensions.
#     if '.csv' in filename:
#         df = pd.read_csv(
#             io.StringIO(decoded.decode('utf-8')))
#     df_proj = df[['events.confirmed.date']]
#
#     df_proj['count'] = np.zeros(len(df_proj))
#     df_proj = df_proj.groupby(['events.confirmed.date']).count()
#     fig = px.line(df_proj)
#     return html.Div([
#         html.H5(filename),
#         html.H6(datetime.datetime.fromtimestamp(date)),
#         dcc.Graph(figure=fig),
#
#         html.Hr(),  # horizontal line
#
#         # For debugging, display the raw contents provided by the web browser
#         html.Div('Raw Content'),
#         html.Pre(contents[0:200] + '...', style={
#             'whiteSpace': 'pre-wrap',
#             'wordBreak': 'break-all'
#         })
#     ])


@app.callback(Output('output-data-upload', 'children'),
              Input('upload-data', 'contents'),
              State('upload-data', 'filename'),
              State('upload-data', 'last_modified'))
def update_output(list_of_contents, list_of_names, list_of_dates):
    if list_of_contents is not None:
        children = [
            parse_contents_india_data(c, n, d) for c, n, d in
            zip(list_of_contents, list_of_names, list_of_dates)]
        return children


@app.callback(
    Output('output-container-date-picker-range', 'children'),
    Input('my-date-picker-range', 'start_date'),
    Input('my-date-picker-range', 'end_date'))
def update_output_with_date_selection(start_date, end_date):
    """start_date and end_date are strings."""
    s = datetime.datetime.fromisoformat(start_date)
    e = datetime.datetime.fromisoformat(end_date)

    # TODO(sarisht): Would run Time series and extract top-K contributors.

    # TODO(rajiv): Would then run clustering on it based on the neighborhood
    #  criteria.

    # TODO(rajiv, sarisht): Finally we return the best explanation among those
    #  clusters.



@app.callback(
    Output('output-neighborhood', 'children'),
    Input('neighborhood-criteria', 'neighborhood_criteria'))
def update_output_with_neighborhood_criteria(neighborhood_criteria):
    return


app.layout = html.Div([
    html.H1("DB Explain"),
    dcc.Upload(id='upload-data', children=[html.Button('Upload File')],
               multiple=True),
    html.Hr(),
    dcc.Dropdown(['None', 'location', 'vaccination rate'], 'None',
                 id='neighborhood-criteria'),
    html.Div(id='output-data-upload'),
    dcc.DatePickerRange(
        id='my-date-picker-range',
        min_date_allowed=date(1995, 8, 5),
        max_date_allowed=date(2017, 9, 19),
        initial_visible_month=date(2017, 8, 5),
        end_date=date(2017, 8, 25)
    ),
    html.Div(id='output-container-date-picker-range')
])

if __name__ == '__main__':
    app.run_server(debug=True)
    # df = load_csv('IN.csv')
    
