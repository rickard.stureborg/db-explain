import base64
import re
import datetime
import io
import os
import sys

import numpy as np
from datetime import date
from dash import Dash, html, dcc, dash_table
import plotly.express as px
import pandas as pd
from dash.dependencies import Input, Output, State
from utils.data_utils import load_data, load_preprocess_india_data
from utils.mongo_utils import get_pymongo_client
from utils.datetime_utils import date2ts

external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']

app = Dash(__name__,
           external_stylesheets=external_stylesheets)
TS_COL_NAME = 'Active'


def read_tsexplain(filepath):
    with open(filepath, 'r') as fin:
        lines = fin.readlines()
        lines = [line.strip() for line in lines]

    candidates = []

    for line in lines:
        splits = line.split(',')
        district = splits[0]
        score = int(splits[1].strip())
        candidates.append((district, score))

    return candidates

def cluster_by_district_vaccination_rate(candidates, start_date, end_date,
                                         delta=5, max_cluster_size=100):

    dbname = 'dbexplain'
    collection = 'district_vaccination_new'
    client = get_pymongo_client()
    db = client.get_database(dbname)
    coll = db.get_collection(collection)
    vax1 = []
    for district, score in candidates:
        if district == 'Unknown':
            continue
        start = coll.find_one({'District': district,
                               'DateTs': date2ts(start_date,
                                                 '%d/%m/%y')})
        # print(start)
        end = coll.find_one({'District': district, 'DateTs': date2ts(end_date,
            '%d/%m/%y')})
        if start is None or end is None:
            continue
        vax1_percentage = ((end['vaccination1'] + start['vaccination1'])/(
                2*start['Population']))*100
        vax1.append((district, vax1_percentage))

    clusters = []
    current_cluster = []
    current_cluster_indices = []
    cluster_root_percentage = vax1[0][1]
    vax1 = vax1[1:]
    while len(vax1) != 0:
        indices = []

        # Consider all the districts in a delta neighborhood of the root
        # district.
        for index, (district, vax1_percentage) in enumerate(vax1):
            if abs(cluster_root_percentage - vax1_percentage) <= delta:
                indices.append(index)
        sub_cluster = []

        # Obtain corresponding indices from the vax1 array.
        for index in indices:
            sub_cluster.append((index, vax1[index][1]))

        # Sort and take the top max_cluster_size outputs.
        sub_cluster.sort(key=lambda x: x[1], reverse=True)
        sub_cluster = sub_cluster[:max_cluster_size-1]

        # Update the current cluster and keep track of the indices added.
        for index, item in sub_cluster:
            current_cluster.append(vax1[index][0])
            current_cluster_indices.append(index)
            print(item)
        print('-----------------')
        # Add cluster to the set of clusters.
        clusters.append(current_cluster)

        # Update vax1 by removing the indices added to the cluster.
        vax1_new = []
        for index, item in enumerate(vax1):
            if index not in current_cluster_indices:
                vax1_new.append(vax1[index])
        vax1 = vax1_new
        if len(vax1) == 0:
            break
        current_cluster = []
        current_cluster_indices = []
        cluster_root_percentage = vax1[0][1]

    return clusters


# def cluster_by_district_vaccination_rate(candidates, start_date, end_date,
#                                          delta=5):
#     """Takes the candidates from ts explain along with their scores and
#        clusters them to form clusters.
#        Arguments:
#            candidates: a list of (district, score) tuples from TSExplain
#        Returns:
#            ret: list of ([cluster_elem1, cluster_elem2, ...], score) tuples.
#        """
#     dbname = 'dbexplain'
#     collection = 'district_vaccination_new'
#     client = get_pymongo_client()
#     db = client.get_database(dbname)
#     coll = db.get_collection(collection)
#     vax1 = []
#     for district, score in candidates:
#         if district == 'Unknown':
#             continue
#         # print(district)
#         start = coll.find_one({'District': district,
#                                'DateTs': date2ts(start_date,
#                                                  '%d/%m/%y')})
#         # print(start)
#         end = coll.find_one({'District': district, 'DateTs': date2ts(end_date,
#             '%d/%m/%y')})
#         # print(end)
#         if start is None or end is None:
#             continue
#         # We are just getting mean.
#         # print(f'end: {end["vaccination1"]}, start: {start["vaccination1"]}, '
#         #       f'population: {start["Population"]}')
#         vaccinations1_percentage = (((end['vaccination1'] + start[
#             'vaccination1']) / (start['Population'] + 1)) / 2) * 100
#         vax1.append((district, vaccinations1_percentage))
#     vax1.sort(key=lambda x: x[1],
#               reverse=True)
#     # print('vax1', vax1)
#     clusters = []
#     index = 0
#     current_cluster = []
#     cluster_average = 0.0
#     cluster_max = vax1[0][1]
#     while index < len(vax1) :
#         if abs(cluster_max - vax1[index][1]) <= delta:
#             current_cluster.append(vax1[index][0])
#             cluster_average += vax1[index][1]
#             index += 1
#         else:
#             # print(f'current_cluster: {current_cluster} ')
#             clusters.append((current_cluster,
#                              cluster_average / len(current_cluster)))
#             current_cluster = [vax1[index][0]]
#             cluster_average = vax1[index][1]
#             cluster_max = vax1[index][1]
#             index += 1
#     clusters.append((current_cluster, cluster_average / len(current_cluster)))
#     return clusters


def build_ts_graph(df, time_column='Date', ts_column='Active'):
    df_ts = df[[time_column, ts_column]]
    df_ts = df_ts.groupby(['Date']).sum()
    fig = px.line(df_ts)
    return html.Div([dcc.Graph(figure=fig), html.Hr()])


def update_output():
    client = get_pymongo_client()
    db = client.get_database('dbexplain')
    coll = db.get_collection('state')
    records = coll.find()
    data = []
    for record in records:
        data.append([record['Date'], record['State'], record['Confirmed'],
                     record['Recovered'], record['Deceased'], record['Active']])
    df = pd.DataFrame(data,
                      columns=['Date', 'State', 'Confirmed', 'Recovered',
                               'Deceased', 'Active'])

    children = [build_ts_graph(df,
                               'Date',
                               'Active')]

    return children


@app.callback(Output('output-container-date-picker-range',
                     'children'),
              Input('my-date-picker-range',
                    'start_date'),
              Input('my-date-picker-range',
                    'end_date'))
def update_output_with_date_selection(start_date, end_date):
    """start_date and end_date are strings."""
    # print(start_date, type(start_date))
    if start_date == 'None' or end_date == 'None':
        return []
    s = datetime.datetime.fromisoformat(str(start_date))
    e = datetime.datetime.fromisoformat(str(end_date))

    # TODO(sarisht): Would run Time series and extract top-K contributors.

    # TODO(rajiv): Would then run clustering on it based on the neighborhood  #  criteria.

    # TODO(rajiv, sarisht): Finally we return the best explanation among those  #  clusters.


@app.callback(Output('output-neighborhood',
                     'children'),
              Input('neighborhood-criteria',
                    'value'))
def update_output_with_neighborhood_criteria(value):
    print(value)
    if value == 'vaccination rate':
        print('Selected vaccination rate.')
    children = [html.Div([html.Hr()])]
    return children


app.layout = html.Div([html.H1("DB Explain"),

                       html.Div(id='output-data-upload',
                                children=update_output()),
                       dcc.Dropdown(['None', 'location', 'vaccination rate'],
                                    'None',
                                    id='neighborhood-criteria'),
                       dcc.DatePickerRange(id='my-date-picker-range',
                                           min_date_allowed=date(1995,
                                                                 8,
                                                                 5),
                                           max_date_allowed=date(2017,
                                                                 9,
                                                                 19),
                                           initial_visible_month=date(2017,
                                                                      8,
                                                                      5),
                                           end_date=date(2017,
                                                         8,
                                                         25)),
                       html.Div(id='output-container-date-picker-range'),
                       html.Div(id='output-neighborhood')])

if __name__ == '__main__':
    # app.run_server(debug=True)  # df = load_csv('IN.csv')

    # start_date = sys.argv[1]
    # end_date = sys.argv[2]
    # input_file = sys.argv[3]
    # output_file = sys.argv[4]
    # # start_date = '22/09/21'
    # # end_date = '31/01/21'
    # # input_file = '/Users/rajiv/Desktop/db-explain/data/tsexplain.csv'
    # # output_file = '/Users/rajiv/Desktop/db-explain/data/220921_311021.csv'
    input_file = sys.argv[1]
    print(input_file)
    date_str = re.findall(r'[0-9]+', input_file)[0]
    y = date_str[0:4]
    s_m = date_str[4:6]
    s_d = date_str[6:8]
    e_m = date_str[8:10]
    e_d = date_str[10:12]
    start_date = f'{s_d}/{s_m}/{y[2:4]}'
    end_date = f'{e_d}/{e_m}/{y[2:4]}'
    output_file = input_file.replace('/input/', '/output/')

    candidates = read_tsexplain(input_file)
    clusters = cluster_by_district_vaccination_rate(candidates,
                                                    start_date,
                                                    end_date,
                                                    delta=5,
                                                    max_cluster_size=5)
    print(clusters)
    start_fmt = start_date.split('/')
    end_fmt = end_date.split('/')
    with open(output_file, 'w') as fout:
        for iter, cluster in enumerate(clusters):
            districts = cluster
            print(f'cluster: {iter+1}: {districts}')
            for district in districts:
                fout.write(district + ',' + str(iter + 1) + '\n')
