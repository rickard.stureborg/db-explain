import datetime
import pandas as pd
from datetime import timedelta


def date_belongs_to_range(date, start, end):
    """All the arguments should be of type datetime.datetime objects."""
    date_ts = datetime.datetime.timestamp(date)
    start_ts = datetime.datetime.timestamp(start)
    end_ts = datetime.datetime.timestamp(end)
    return (date_ts >= start_ts) and (date_ts <= end_ts)


def subtract_one_day(date_str):
    subtracted_date = pd.to_datetime(date_str) - timedelta(days=1)
    subtracted_date = subtracted_date.strftime("%Y-%m-%d")
    return subtracted_date


def date2ts(date, format_str):
    """Convert date string to a time stamp."""
    dt = datetime.datetime.strptime(date, format_str)
    date_ts = datetime.datetime.timestamp(dt)
    return date_ts
