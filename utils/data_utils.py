import os
import sys
import json
import pandas as pd
from tqdm import tqdm
from datetime import timedelta
from utils.datetime_utils import subtract_one_day
import pickle


def read_population_jsonfile(json_filepath):
    d = {}
    with open(json_filepath, 'r') as fin:
        d = json.load(fin)
    return d


def load_data(csv_filepath):
    df = pd.read_csv(csv_filepath, delimiter=',')
    print(df.columns)
    return df


def load_preprocess_india_data(csv_filepath):
    """Calculate the active cases using the existing data."""
    if os.path.exists('data/india_data.pkl'):
        with open('data/india_data.pkl', 'rb') as fin:
            df = pickle.load(fin)
    else:
        df = load_data(csv_filepath)
        df['Active'] = df['Confirmed'] - df['Recovered']
        for it, row in tqdm(df.iterrows()):
            try:
                date = row['Date']
                state = row['State']
                district = row['District']
                date_minus_day = subtract_one_day(date)
                prev_active = df.loc[(df['Date'] == date_minus_day) &
                                     (df['State'] == state) &
                                     (df['District'] == district)]
                curr_active = prev_active + row['Confirmed'] - \
                              row['Recovered'] - \
                              row['Deceased']
                row['Active'] = curr_active
            except:
                continue
        with open('data/india_data.pkl', 'wb') as fout:
            pickle.dump(df, fout)

    return df


def df2dict(df):
    d = {}
    for iter, row in tqdm(df.iterrows()):
        date = row['Date']
        state = row['State']
        district = row['District']
        if date not in d.keys():
            d[date] = {}
        if state not in d[date].keys():
            d[date][state] = {}
        if district not in d[date][state].keys():
            d[date][state][district] = {
                'Confirmed': row['Confirmed'],
                'Recovered': row['Recovered'],
                'Deceased': row['Deceased'],
                'Active': row['Active']
            }
    return d


def dict2df(d):
    data = []
    for date in d.keys():
        for state in d[date].keys():
            for district in d[date][state].keys():
                data.append([date, state, district, district['Confirmed'],
                           district['Recovered'], district['Deceased'],
                           district['Active']])
    df = pd.DataFrame(data, columns=['Date', 'State', 'District', 'Confirmed',
                                   'Recovered', 'Deceased', 'Active'])
    return df


if __name__ == '__main__':
    # df = load_data('/Users/rajiv/Desktop/db-explain/data/districts.csv')
    df = load_preprocess_india_data(
        '/Users/rajiv/Desktop/db-explain/data/districts.csv')
    print(df)
