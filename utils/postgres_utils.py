import psycopg2


def get_new_connection(dbname='db', user='postgres'):
    conn = psycopg2.connect(f"dbname='{dbname}' user='{user}")
    cursor = conn.cursor()
    return cursor


def execute_query(cursor=None, query=''):
    """Executes the query and returns the records."""
    cursor.execute(query)
    records = cursor.fetchall()
    return records
