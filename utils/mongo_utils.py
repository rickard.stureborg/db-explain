from pymongo import MongoClient


def get_pymongo_client(host='localhost', port=27017):
    client = MongoClient(host, port)
    return client


if __name__ == '__main__':
    client = get_pymongo_client()
    print(client.server_info())