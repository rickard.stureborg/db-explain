
# Project Time-series

## Members' contributions:

Sarisht - Examining data, finding useful motivating examples in COVID India data. Running baseline TSExplain and adapting and integrating it into our workflow.

Rajiv - Building out code-base for algorithms and visualization to help with debugging/improvements. Created UI component and handled data aggregation to feed the algorithm.

Oscar - Formalizing project motivation and problem. Collecting relevant literature and results for project report and final presentation.

## Code

https://gitlab.cs.duke.edu/rickard.stureborg/db-explain

Instructions for running:

1. Install MongoDB

    ibrew install mongodb-atlas-cli

    ibrew install mongodb

    brew tap mongodb/brew

    ibrew install mongodb-community@5.0

    brew services start mongodb/brew/mongodb-community

2. Download data_backup.zip

3. Uncompress the file, there’ll be a folder named `data_backup`

4. Run mongorestore -d dbexplain data_backup

5. Make sure you have mongodb server up and running.

6. Install MongoCompass if you want GUI.

---

First we update the collections, run the following command. Uncomment the code under the “__main__” function
 
`/Users/rajiv/Desktop/venv/bin/python /Users/rajiv/Desktop/db-explain/preprocess.py`
 
Run TSExplain and obtain top-K candidates.
 
This code then reads the output from TSExplain and clusters it.

``` 
python3 /Users/rajiv/Desktop/db-explain/ui/main.py 01/04/21 30/04/21 \
<output_from_TSExplain_as_input> \
<output_clusters>
```
 
Re-run TS Explain with updated granularity (in terms of clusters instead of districts) and re-rank the clusters
