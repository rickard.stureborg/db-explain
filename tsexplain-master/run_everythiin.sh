python pre-pre-preproocess.py $1

echo "dataset: covid
granularity: day
tsv_file: ${PWD%/}/districts_mod_active_$1.tsv
feature_hier:
  - features:
    - county
time_cols:
  - month
  - day
val_col: total-confirmed-cases
try_seg_len_ratio: 0.05
try_total_len_ratio: 3
supp_ratio: 0.001
time_min: $2 $3
time_max: $4 $5
post_process: 0
explain_time_min: $2 $3
explain_time_max: $4 $5
metric: MChangeAbs
topK: 200
pred_len: 1
sim: mutual
seg_number: 1
min_len: 2
min_dist: 0
cascading_opt: 1" > covid_init.yaml

echo "dataset: covid
granularity: day
tsv_file: ${PWD%/}/districts_mod_active_$1.tsv
feature_hier:
  - features:
    - county
time_cols:
  - month
  - day
val_col: total-confirmed-cases
try_seg_len_ratio: 0.05
try_total_len_ratio: 3
supp_ratio: 0.001
time_min: $2 $3
time_max: $4 $5
post_process: 0
explain_time_min: $2 $3
explain_time_max: $4 $5
metric: MChangeAbs
topK: 1
pred_len: 3
sim: mutual
seg_number: 2
min_len: 2
min_dist: 0
cascading_opt: 1" > covid_theirs.yaml

echo "dataset: covid
granularity: day
tsv_file: ${PWD%/}/districts_mod_active_segmented_$1.tsv
feature_hier:
  - features:
    - county
time_cols:
  - month
  - day
val_col: total-confirmed-cases
try_seg_len_ratio: 0.05
try_total_len_ratio: 3
supp_ratio: 0.001
time_min: $2 $3
time_max: $4 $5
post_process: 0
explain_time_min: $2 $3
explain_time_max: $4 $5
metric: MChangeAbs
topK: 1
pred_len: 1
sim: mutual
seg_number: 2
min_len: 2
min_dist: 0
cascading_opt: 1" > covid_ours.yaml

cd build
cmake ..
make
cd ../script

python3 RunExp.py ../covid_init.yaml

mv outputext.json ../whateverrajivwants$1$2$3$4$5.csv

# ''' insert rajivs code '''
cd ..

python3 post_segmentation.py $1

cd script/

python3 RunExp.py ../covid_ours.yaml
mv output.json ../ours$1$2$3$4$5.json

python3 RunExp.py ../covid_theirs.yaml
mv output.json ../theirs$1$2$3$4$5.json