import csv
import sys
_year = int(sys.argv[1])
tsvfile = open('districts_mod_active_'+str(_year)+'.tsv','w')
a = 'date	week	year	month	day	week_number	county	datestamp	index	total-confirmed-cases	state	daily-confirmed-cases	agg_2_days	agg_3_days	agg_4_days	agg_5_days	agg_6_days	agg_7_days'.split()
with open('districts.csv') as csv_file:
    csv_reader = csv.reader(csv_file, delimiter=',')
    writer = csv.writer(tsvfile, delimiter='\t')
    line_count = 0
    week = 1
    curr_day = -1
    index = 1
    prev_day = -1
    for row in csv_reader:
        if line_count == 0:
            print(row)
            writer.writerow(a)
            line_count += 1
            
        else:
            [year,month,day] = map(int,row[0].split('-'))
            if year != _year: continue
            # if index == 366: break
            date = '-'.join(map(str,[month,day]))
            if day != prev_day:
                prev_day = day
                curr_day += 1
                index+=1
                if curr_day == 7:
                    curr_day = 0
                    week+=1
            county=row[2]
            datestamp=row[0]
            confirmed=str(int(row[3])-int(row[4])-int(row[5]))
            state=row[1]
            writer.writerow([date,week,year,month,day,week,county,datestamp,index,confirmed,state]+[confirmed]*7)
            line_count += 1
tsvfile.close()