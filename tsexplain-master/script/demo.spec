tsv_file: demo_data.tsv
feature_hier_number: 1
feature_number: 1
state
time_col_number: 1
index
try_seg_len_ratio: 0.05
try_total_len_ratio: 3
val_col: daily-confirmed-cases
supp_ratio: 0
time_min: 21
time_max: 365
post_process: none
explain_time_min: 21
explain_time_max: 365
metric: MChangeAbs
topK: 3
pred_len: -1
sim: mutual
seg_number: -10
min_len: 2
min_dist: 0
cascading_opt: 1
