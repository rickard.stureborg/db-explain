tsv_file: /Users/sarisht/Documents/Duke-Assignments/tsexplain-master/script/districts_mod_active_segmented_2020.tsv
feature_hier_number: 1
feature_number: 1
county
time_col_number: 2
month
day
try_seg_len_ratio: 0.05
try_total_len_ratio: 3
val_col: total-confirmed-cases
supp_ratio: 0.001
time_min: 09 22
time_max: 10 31
post_process: 0
explain_time_min: 09 22
explain_time_max: 10 31
metric: MChangeAbs
topK: 1
pred_len: 1
sim: mutual
seg_number: 2
min_len: 2
min_dist: 0
cascading_opt: 1
